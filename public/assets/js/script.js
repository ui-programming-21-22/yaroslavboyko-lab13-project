//declaration of variables ["*>=<*"]
const startButton = document.getElementById('startBtn')
const nextButton = document.getElementById('nextBtn')
const questionContainerElement = document.getElementById('question-container')
const questionElement = document.getElementById('questions')
const answerButtonElent = document.getElementById('answerButtons')

let moveQuestions, currentQuestionsIndex

startButton.addEventListener('click', startGame)
nextButton.addEventListener('click', ()=>{currentQuestionsIndex++ ,setNextQuestion()})


function startGame()
{

    var audio = new Audio('Aphex Twin - Just Fall Asleep (1080p HD_HQ).mp3')
    audio.play()

    startButton.classList.add('hide')
    moveQuestions = questions.sort(()=> Math.random() - .5)
    currentQuestionsIndex = 0
    questionContainerElement.classList.remove('hide')
    setNextQuestion()
}

function setNextQuestion()
{
    resetState()
    showQuestion(moveQuestions[currentQuestionsIndex])
}

function showQuestion(question)
{
    questionElement.innerText = question.question
    question.answers.forEach(answer => {
        const button = document.createElement('button')
        button.innerText = answer.text
        button.classList.add('btn')
        if(answer.correct)
        {
            button.dataset.correct = answer.correct
        }
        button.addEventListener('click', selectAnswer)
        answerButtonElent.appendChild(button)
    })
}

function resetState()
{
    clearStatusClass(document.body)
    nextButton.classList.add('hide')
    while(answerButtonElent.firstChild)
    {
        answerButtonElent.removeChild(answerButtonElent.firstChild)
    }
}

function selectAnswer(e)
{
    const selectedButton = e.target
    const correct = selectedButton.dataset.correct
    setStatusClass(document.body, correct)
    Array.from(answerButtonElent.children).forEach(button => {setStatusClass(button, button.dataset.correct)})
    if(moveQuestions.length > currentQuestionsIndex + 1)
    {
        nextButton.classList.remove('hide')
    }
    else{
        startButton.innerHTML = 'Restart'
        startButton.classList.remove('hide')
    }
}


function setStatusClass(element, correct)
{
    clearStatusClass(element)
    if(correct)
    {
        element.classList.add('correct')
    }
    else{
        element.classList.add('wrong')
    }
}

function clearStatusClass(element)
{
    element.classList.remove('correct')
    element.classList.remove('correct')
}

const questions = [
    {
        question: 'WHAT IS NELUMBO NUCIFERA',
        answers: [
            {text: 'LOTUS', correct: true},
            {text: 'ALOE', correct:false}
        ]
    },
    {
        question: 'WHAT IS PYRUS MALUS ',
        answers: [
            {text: 'APPLE', correct: true},
            {text: 'LEMON', correct: false}
        ]
    },

    {
        question: ' WHAT IS ZEA MAYS',
        answers: [
            {text: 'RICE', correct: false},
            {text: 'CORN', correct:true},
            {text: 'SOYBEAN', correct:false}
        ]
    },

    {
        question: 'WHAT IS TRITICUM AESTIVUM',
        answers: [
            {text: 'MUNGBEAN', correct:false},
            {text: 'LIME', correct: false},
            {text: 'WHEAT', correct: true}
        ]
    }
    
]



function movingblob(){
    var elem = document.getElementById('blob');
    var pos = 0;

    clearInterval(id);
    id = setInterval(frame,10);
    function frame() {
        if(pos == 350)
        {
            clearInterval(id);
        }
        else{
            pos++;
            elem.style.top = pos + "px";
            elem.style.left = pos + "px";
        }
    }
}